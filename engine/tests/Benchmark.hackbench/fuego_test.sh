tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1

function test_pre_check {
    assert_define BENCHMARK_HACKBENCH_PARAMS
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 hackbench
}

function test_deploy {
    put hackbench  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./hackbench $BENCHMARK_HACKBENCH_PARAMS"
}
