===========
Description
===========
Obtained from fanotify07.c DESCRIPTION:

Check that fanotify permissions events are handled properly on
instance destruction.

A rough outline of this testcase is:
 * setup() creates a file fname_<pid>
 * an fanotify_mark is set on the file
 * create 16 children
   * each child generates a bunch of fanotify events
     * the file is opened, and reads are done in an infinite loop
 * read notifications events
   * 95% of the time, respond to the notification event by writing back
 * try to set up another notification instance
 * if everthing works, return PASS
 * clean() closes the fd_notify file

This test catches a bug that was present in the 4.9 kernel.
The issue was fixed in kernel version 4.14.

On kernels before then, it might cause a kernel panic,
or leave fanotify07 processes in an unkillable zombie state.

Kernel crashes should be fixed by commit:
  96d41019e3ac "fanotify: fix list corruption in fanotify_get_response()"

Kernel hangs should be fixed by commit:
  05f0e38724e8 "fanotify: Release SRCU lock when waiting for userspace response"

====
Tags
====

 * kernel, syscall, fanotify

=========
Resources
=========
https://www.spinics.net/lists/linux-fsdevel/msg109131.html

=======
Results
=======
This test times out on a Renesas R-Car-M3 Starter Kit, running
kernel version 4.9.0

There were 17 fanotify07 processes still running on the machine
(in state "uninterruptible sleep") that could not be killed.

On a BeagleBone Black running kernel version 3.8.13-bone50,
it results in a kernel panic. Here is an example:

  [183840.529408] INFO: task fsnotify_mark:40 blocked for more than 60 seconds.
  [183840.536791] "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
  [183840.545286] Kernel panic - not syncing: hung_task: blocked tasks
  [183840.551795] [<c00111f1>] (unwind_backtrace+0x1/0x9c) from [<c04c8955>] (panic+0x59/0x158)
  [183840.560585] [<c04c8955>] (panic+0x59/0x158) from [<c00724f9>] (watchdog+0x19 d/0x1c0)
  [183840.568909] [<c00724f9>] (watchdog+0x19d/0x1c0) from [<c0045103>] (kthread+0 x6b/0x78)
  [183840.577320] [<c0045103>] (kthread+0x6b/0x78) from [<c000c8fd>] (ret_from_for k+0x11/0x34)
  [183840.585989] drm_kms_helper: panic occurred, switching back to text console

.. fuego_result_list::

======
Status
======

.. fuego_status::

=====
Notes
=====
