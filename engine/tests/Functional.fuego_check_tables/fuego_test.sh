function test_pre_check {
    assert_define FUNCTIONAL_FUEGO_CHECK_TABLES_TESTCASE_SET
}

function test_run {
    # now do some host-driven tests
    tmpfile=$(mktemp)

    # output result in TAP format
    if [ "$FUNCTIONAL_FUEGO_CHECK_TABLES_TESTCASE_SET" = "1" ] ; then
        echo "1..6" >$tmpfile
        echo "ok 1 testset1 testcase1 - try success" >>$tmpfile
        echo "ok 2 testset1 testcase2 - another success" >>$tmpfile
        echo "not ok 3 testset1 testcase3 - a failure" >>$tmpfile
        echo "ok 4 testset2 subtest1 - testset2 success" >>$tmpfile
        echo "not ok 5 testset2 subtest2 - a failure in ts2" >>$tmpfile
        echo "not ok 6 testset2 subtest3 - another ts2 failure" >>$tmpfile
    else
        echo "1..6" >$tmpfile
        echo "ok 1 testset1 testcase1 - try success" >>$tmpfile
        echo "ok 2 testset1 testcase2 - another success" >>$tmpfile
        echo "not ok 3 testset1 testcase3 - a failure" >>$tmpfile
        echo "ok 4 altset1 subtest1 - altset1 success" >>$tmpfile
        echo "ok 5 altset1 subtest2 - altset1 success2" >>$tmpfile
        echo "ok 6 altset2 subtest1 - altset2 success3" >>$tmpfile
    fi
    echo "Test done" >>$tmpfile

    log_this "cat $tmpfile"
    rm "$tmpfile"
}

function test_processing {
    log_compare "$TESTDIR" "3" "^ok" "p"
    log_compare "$TESTDIR" "3" "^not ok" "n"
}
