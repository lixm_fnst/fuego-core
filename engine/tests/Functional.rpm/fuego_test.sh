function test_pre_check {
    is_on_target_path rpm PROGRAM_RPM
    assert_define PROGRAM_RPM
}

function test_deploy {
    put $TEST_HOME/rpm_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $TEST_HOME/test-manual-1.2.3.noarch.rpm $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        sh -v rpm_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
