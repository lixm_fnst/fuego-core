FUNCTIONAL_KERNEL_BUILD_PER_JOB_BUILD="true"

function test_pre_check {
    echo "Doing a pre_check"
    # FIXTHIS: if making uImage, check for mkimage

    if [ "$NODE_NAME" != "docker" ]; then
        abort_job "This test can only run on docker currently."
    fi

    if [ -z "$FUNCTIONAL_KERNEL_BUILD_ARCH" ]; then
        FUNCTIONAL_KERNEL_BUILD_ARCH="x86_64"
    fi

    if [ "$FUNCTIONAL_KERNEL_BUILD_ARCH" = "x86_64" ]; then
        is_on_sdk libelf.a LIBELF /usr/lib/$FUNCTIONAL_KERNEL_BUILD_ARCH-linux-*/
        assert_define LIBELF
        is_on_sdk libssl.a LIBSSL /usr/lib/$FUNCTIONAL_KERNEL_BUILD_ARCH-linux-*/
        assert_define LIBSSL
        is_on_sdk bison PROGRAM_BISON /usr/bin/
        assert_define PROGRAM_BISON
        is_on_sdk flex PROGRAM_FLEX /usr/bin/
        assert_define PROGRAM_FLEX
    fi
}

function test_run {
    # make sure we have the latest source code.
    cd "$WORKSPACE/$JOB_BUILD_DIR"
    if [ -d ".git" ]; then
        git pull || echo "WARNING: git pull failed"
    else
        echo "WARNING: no git repository, assuming you used a tarball"
    fi

    if [ -z "$FUNCTIONAL_KERNEL_BUILD_CONFIG" ]; then
        FUNCTIONAL_KERNEL_BUILD_CONFIG="defconfig"
    fi
    echo "Configuring kernel with $FUNCTIONAL_KERNEL_BUILD_CONFIG"
    make ARCH=$FUNCTIONAL_KERNEL_BUILD_ARCH $FUNCTIONAL_KERNEL_BUILD_CONFIG

    # Building
    echo "Building Kernel"
    if [ -z "$FUNCTIONAL_KERNEL_BUILD_PARAMS" ]; then
        FUNCTIONAL_KERNEL_BUILD_PARAMS="-j$(nproc) bzImage modules"
    fi

    if [ ! -z "$FUNCTIONAL_KERNEL_BUILD_PLATFORM" ]; then
          OLD_PLATFORM=$PLATFORM
          PLATFORM=$FUNCTIONAL_KERNEL_BUILD_PLATFORM
          source $FUEGO_RO/toolchains/tools.sh
          PLATFORM=$OLD_PLATFORM
    fi

    make ARCH=$FUNCTIONAL_KERNEL_BUILD_ARCH $FUNCTIONAL_KERNEL_BUILD_PARAMS 2>&1 | tee build.log || true

    # cat on target populates the testlog with build.log
    report "cat $WORKSPACE/$JOB_BUILD_DIR/build.log"

    if [ -z "$FUNCTIONAL_KERNEL_DEPLOY" ]; then
        FUNCTIONAL_KERNEL_DEPLOY="$LOGDIR"
    fi

    # FIXTHIS: output the modules and dtbs as well
    echo "Deploying kernel to $FUNCTIONAL_KERNEL_DEPLOY"
    cp $WORKSPACE/$JOB_BUILD_DIR/arch/$FUNCTIONAL_KERNEL_BUILD_ARCH/boot/*Image $FUNCTIONAL_KERNEL_DEPLOY
}

function test_processing {
    echo "Processing kernel build log"
    if [ -z "$FUNCTIONAL_KERNEL_BUILD_REGEX_P" ]; then
        log_compare "$TESTDIR" "1" "[ \t]*Kernel: arch/.* is ready" "p"
    else
        log_compare "$TESTDIR" "1" "$FUNCTIONAL_KERNEL_BUILD_REGEX_P" "p"
    fi
}


