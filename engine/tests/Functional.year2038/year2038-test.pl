#!/usr/bin/perl
# test program source (year2038-test.pl) came from:
# https://askubuntu.com/questions/299475/will-the-linux-clock-fail-a-january-19-2038-31408
#
# It's unclear exactly what's getting tested here, but this does fail
# on some older versions of Debian. (32-bit systems)
#
# It appears to test the time conversion of the perl ctime function.
# This is probably also testing the C library ctime function.
# But it doesn't appear to test any kernel timekeeping syscalls.
#
use POSIX;
$ENV{'TZ'} = "GMT";
for ($clock = 2147483641; $clock < 2147483651; $clock++) {
    print ctime($clock);
}
