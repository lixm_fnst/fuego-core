tarball=gtkperf-0.40.tar.bz2

function test_build {
    cd src
    patch -p0 -N -s < $TEST_HOME/gtkperf_callbacks.c.patch
    patch -p0 -N -s < $TEST_HOME/gtkperf_interface.c.patch
    patch -p0 -N -s < $TEST_HOME/gtkperf_main.c.patch
    patch -p0 -N -s < $TEST_HOME/gtkperf_appdata.h.patch
    cd ..
    export PATH=/usr/local/bin:$PATH
    # get updated config.sub and config.guess files, so configure
    # doesn't reject newer toolchains
    cp /usr/share/misc/config.{sub,guess} .
    ./configure --prefix=$BOARD_TESTDIR/$TESTDIR --host=$HOST --build=`uname -m`-linux-gnu --target=$HOST
    make
}

function test_deploy {
    cmd "gdk-pixbuf-query-loaders > /etc/gtk-2.0/gdk-pixbuf.loaders && pango-querymodules > /etc/pango/pango.modules && mkdir -p $BOARD_TESTDIR/fuego.$TESTDIR/share/pixmaps"
    put src/gtkperf  $BOARD_TESTDIR/fuego.$TESTDIR/
    put pixmaps/*.png  $BOARD_TESTDIR/fuego.$TESTDIR/share/pixmaps
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1,a,\"x\"); exit(system(\"./gtkperf -a -x\" a[1]\" -y\" a[2]))}'"
}

function test_cleanup {
    kill_procs gtkperf
}
