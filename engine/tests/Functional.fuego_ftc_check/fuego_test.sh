# Functional.fuego_ftc_check - fuego_test.sh
# This test is used to test some features of the 'ftc' program
# Specifically, this tests the following:
# 1 - whether ftc can be called from inside a test
# 2 - it shows how to call 'ftc wait-for' with a board operation
# 3 - whether a test can set a dynamic board variable
#
function test_run {

    # now do some host-driven tests
    # see if ftc can be called from a test
    tmpfile=$(mktemp)

    echo "echo Starting ftc_check" >$tmpfile
    echo "Here is 'ftc help'" >>$tmpfile
    ftc help >>$tmpfile
    desc="ftc help shows usage"
    if grep Usage: $tmpfile ; then
      echo "ok 1 $desc" >>$tmpfile
    else
      echo "not ok 1 $desc" >>$tmpfile
    fi

    # try using ftc wait-for on target
    echo "--------" >>$tmpfile
    echo "Here are some results from 'ftc wait-for'" >>$tmpfile

    # we expect some of these to fail
    set +e
    start_time=$(date +"%s.%N")
    ftc wait-for -t 30 "/bin/bash -c \"source $LOGDIR/prolog.sh ; ov_transport_cmd true\"" >>$tmpfile
    end_time=$(date +"%s.%N")
    duration=$(python -c "print \"%.2f\" % ($end_time - $start_time)")
    echo "'ftc wait-for true' on target took $duration seconds" >>$tmpfile
    dur_int=$(python -c "print int($end_time - $start_time)")
    desc="ftc wait-for true on target finishes quickly"
    if [ $dur_int -lt 3 ] ; then
      echo "ok 2 $desc" >>$tmpfile
    else
      echo "not ok 2 $desc" >>$tmpfile
    fi

    start_time=$(date +"%s.%N")
    ftc wait-for -t 30 "/bin/bash -c \"source $LOGDIR/prolog.sh ; ov_transport_cmd false\"" >>$tmpfile
    end_time=$(date +"%s.%N")
    duration=$(python -c "print \"%.2f\" % ( $end_time - $start_time )")
    echo "'ftc wait-for false' on target took $duration seconds" >>$tmpfile
    dur_int=$(python -c "print int($end_time - $start_time)")
    desc="ftc wait-for false on target times out"
    if [[ $dur_int -gt 29 && $dur_int -lt 40 ]] ; then
      echo "ok 3 $desc" >>$tmpfile
    else
      echo "not ok 3 $desc" >>$tmpfile
    fi

    echo "Doing check for /tmp/foo_check on target!!"
    start_time=$(date +"%s.%N")
    ftc wait-for -t 30 "/bin/bash -c \"source $LOGDIR/prolog.sh ; ov_transport_cmd test -f /tmp/foo_check\"" >>$tmpfile
    end_time=$(date +"%s.%N")
    duration=$(python -c "print \"%.2f\" % ( $end_time - $start_time )")
    desc="ftc wait-for test -f /tmp/foo_check"
    echo "$desc on target took $duration seconds" >>$tmpfile
    dur_int=$(python -c "print int($end_time - $start_time)")
    if [[ $dur_int -gt 29 && $dur_int -lt 40 ]] ; then
      echo "ok 4 $desc" >>$tmpfile
    else
      echo "not ok 4 $desc" >>$tmpfile
    fi

    set -e
    echo "--------" >>$tmpfile
    echo "Here are some results from ftc variable tests" >>$tmpfile
    desc="ftc set-var TEST_VAR1"
    ftc set-var -v bbb TEST_VAR1="foo"
    TEST_VAR1=$(ftc query-board bbb -n TEST_VAR1)
    if [ "$TEST_VAR1" = "foo" ] ; then
      echo "ok 5 $desc" >>$tmpfile
    else
      echo "not ok 5 $desc" >>$tmpfile
    fi

    desc="ftc set-var TEST_VAR2"
    ftc set-var -v bbb TEST_VAR2="bar"
    TEST_VAR2=$(ftc query-board bbb -n TEST_VAR2)
    if [ "$TEST_VAR2" = "bar" ] ; then
      echo "ok 6 $desc" >>$tmpfile
    else
      echo "not ok 6 $desc" >>$tmpfile
    fi

    desc="ftc delete-var TEST_VAR2"
    ftc delete-var -v bbb TEST_VAR2
    if grep TEST_VAR2 $FUEGO_RW/boards/bbb.vars ; then
      echo "not ok 7 $desc" >>$tmpfile
    else
      echo "ok 7 $desc" >>$tmpfile
    fi

    echo Test done >>$tmpfile

    ftc delete-var -v bbb TEST_VAR1

    # put the host log into the target log
    log_this "cat $tmpfile"
    rm "$tmpfile"

}

function test_processing {
    log_compare "$TESTDIR" "7" "^ok" "p"
}
