#!/bin/sh
for i in tests/*.sh; do
    if cat ./skiplist.txt | grep ${i##*/}; then
        echo "Skip test specified in SKIPFILE."
        continue
    fi
    sh $i
done
