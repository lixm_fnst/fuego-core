#!/bin/sh

#  The testscript checks the following options of the command cp
#  1) Option: -i

test="cp"

mkdir test_dir_src
mkdir test_dir_dest
echo "cp test" > test_dir_src/test1
busybox cp test_dir_src/test1 test_dir_dest/
if [ "$(cat test_dir_dest/test1)" = "cp test" ]
then
    echo " -> $test: cp succeeded."
else
    echo " -> $test: cp failed."
    echo " -> &test: TEST-FAIL"
    rm -rf test_dir_src
    rm -rf test_dir_dest
    exit
fi;

yes | busybox cp -i test_dir_dest/test1 test_dir_src/ 2>cp.log
if cat cp.log | grep "cp: overwrite 'test_dir_src/test1'?"
then
    echo " -> $test: TEST-PASS"
else
    echo "-> $test: TEST-FAIL"
fi
rm cp.log
rm -rf test_dir_src
rm -rf test_dir_dest
