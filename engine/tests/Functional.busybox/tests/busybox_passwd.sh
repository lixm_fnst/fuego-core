#!/bin/sh

#  The testscript checks the following options of the command passwd
#  1) Option none

test="passwd"

if [ -e /etc/shadow ]
then
    cp /etc/shadow /etc/shadow.backup
fi

userdel -r test_busybox_passwd
ls /
useradd test_busybox_passwd

expect <<-EOF
spawn busybox.suid passwd test_busybox_passwd
expect "New password:"
send_user " -> $test: busybox.suid passwd test_busybox_passwd executed.\n"
send "test123\r"
expect "Retype password:"
send_user " -> $test: retype passwd executed.\n"
send "test123\r"
expect "Password for test_busybox_passwd changed by root"
send_user " -> $test: TEST-PASS\n"
expect eof
EOF

userdel -r test_busybox_passwd
if [ -e /etc/shadow.backup ]
then
    mv -f /etc/shadow.backup /etc/shadow
fi
