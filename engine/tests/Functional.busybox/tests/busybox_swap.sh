#!/bin/sh

#  The testscript checks the following options of the command swapon&swapoff
#  1) Option -a

test="swapon/swapoff"

function check_if_succeeded {
    if $1 
    then
        echo " -> $test: $1 succeeded."
    else
        echo " -> $test: $1 failed."
        echo " -> $test: TEST-FAIL"
    fi
}

if [ $(cat /proc/swaps | wc -l) = 1 ]
then
    check_if_succeeded "busybox swapon -a"
    check_if_succeeded "busybox swapoff -a"
    if [ $(cat /proc/swaps | wc -l) = 1 ]
    then
        echo " -> $test: TEST-PASS"
    else
        echo " -> $test: TEST-FAIL"
    fi
else
    swap_name=$(cat /proc/swaps | tail -n 1| tr -s " " | cut -d ' ' -f1)
    check_if_succeeded "busybox swapoff $swap_name"
    check_if_succeeded "busybox swapon $swap_name"
    if cat /proc/swaps | grep $swap_name
    then
        echo " -> $test: TEST-PASS"
    else
        echo " -> $test: TEST-FAIL"
    fi
fi;
