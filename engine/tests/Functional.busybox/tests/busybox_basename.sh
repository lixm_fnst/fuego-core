#!/bin/sh

#  The testscript checks the following options of the command basename
#  1) Option none

test="basename"

if [ $(busybox basename ./test_dir/test1) = "test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
