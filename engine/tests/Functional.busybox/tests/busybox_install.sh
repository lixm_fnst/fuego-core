#!/bin/sh

#  The testscript checks the following options of the command install
#  1) Option none

test="install"

mkdir test_dir test_install_dir
echo "test install" > test_dir/test1
busybox install test_dir/test1 test_install_dir
if [ "$(ls test_install_dir)" = "test1" ] && \\
    [ "$(cat test_install_dir/test1)" = "test install" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir test_install_dir;
