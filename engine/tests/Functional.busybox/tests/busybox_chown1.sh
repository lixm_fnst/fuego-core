#!/bin/sh

#  The testscript checks the following options of the command chown
#  1) Option none

test="chown1"

mkdir test_dir
touch test_dir/test1
orig_user=$(id -n -u| cut -b -8)
orig_group=$(id -n -g| cut -b -8)
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,3,4,9 | cut -b 12-)" = "$orig_user $orig_group test1" ]
then
    echo " -> $test: test_dir contents verification succeeded."
else
    echo " -> $test: test_dir contents verification failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir;
    exit
fi;

busybox chown bin ./test_dir/test1
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,3,4,9 | cut -b 12-)" = "bin $orig_group test1" ]
then
    echo " -> $test: test_dir contents verification#2 succeeded."
else
    echo " -> $test: test_dir contents verification#2 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir;
    exit
fi;

busybox chown bin.bin ./test_dir/test1
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,3,4,9 | cut -b 12-)" = "bin bin test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf ./test_dir;
