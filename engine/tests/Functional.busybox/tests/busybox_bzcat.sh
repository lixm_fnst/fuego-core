#!/bin/sh

#  The testscript checks the following options of the command bzcat
#  1) Option none

test="bzcat"

echo "This is a test file">test1
bzip2 test1
if [ "$(busybox bzcat test1.bz2)" = "This is a test file" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test1.bz2;
