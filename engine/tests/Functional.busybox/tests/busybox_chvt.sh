#!/bin/sh

#  The testscript checks the following options of the command chvt
#  1) Option none

test="chvt"

if tty | grep "^not" ; then
    echo " -> $test: test script not running in a changable tty"
    echo " -> $test: TEST-SKIP"
    exit 0
fi

tty_path=$(tty)
# use shell variable sub-string removal to isolate the tty number
anum=${tty_path##*/}
if [ $anum -eq 1 ]
then
    bnum=$(($anum+1))
else
    bnum=$(($anum-1))
fi
echo "$test: Changing to VT $bnum"
if busybox chvt $bnum
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
echo "$test: Changing back to VT $bnum"
busybox chvt $anum
