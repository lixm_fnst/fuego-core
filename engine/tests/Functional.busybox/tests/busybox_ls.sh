#!/bin/sh

#  The testscript checks the following options of the command ls
#  1) Option: -l

test="ls"

mkdir test_dir
touch test_dir/test1 test_dir/test2 test_dir/test3
busybox ls -l ./test_dir > log
if grep "test1" log && grep "test2" log && grep "test3" log
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log;
rm -rf test_dir;
