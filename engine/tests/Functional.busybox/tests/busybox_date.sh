#!/bin/sh

#  The testscript checks the following options of the command date
#  1) Option none

test="date"

export TZ="UTC"
if busybox date -d "@1" | grep "Thu Jan  1 00:00:01 UTC 1970"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
