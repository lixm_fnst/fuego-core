#!/bin/sh

#  The testscript checks the following options of the command ifconfig
#  1) Option none

test="ifconfig"

if busybox ifconfig
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
