#!/bin/sh

#  The testscript checks the following options of the command chmod
#  1) Option: -R

test="chmod2"

mkdir test_dir
touch test_dir/test1
touch test_dir/test2
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,9 > log1

if [ "$(head -n 1 log1 | cut -b 12-16)" = "test1" ] && [ "$(tail -n 1 log1 | cut -b 12-16)" = "test2" ]
then
    echo " -> $test: test_dir contents verification succeeded."
else
    echo " -> $test: test_dir contents verification failed."
    echo " -> $test: TEST-FAIL"
    rm -rf ./test_dir;
    rm log1;
    exit
fi;

busybox chmod -R go-r ./test_dir
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f1,9 > log2
if [ "$(head -n 1 log2 | cut -b 1,2,5,8,12-16)" = "-r--test1" ] && [ "$(tail -n 1 log2 | cut -b 1,2,5,8,12-16)" = "-r--test2" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log1 log2;
rm -rf ./test_dir;
