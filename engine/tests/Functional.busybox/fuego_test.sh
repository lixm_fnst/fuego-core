# $1: string with tests to skip separated by spaces
# $2: absolute path to the skipfile (default: ${LOGDIR}/skiplist.txt)
function skip_tests {
    # split $1 on whitespace, without file globbing
    set -f
    local TESTS=($1)
    local SKIPFILE="${LOGDIR}/skiplist.txt"
    set +f
    echo "Skipping tests: ${TESTS[@]}."
    for testname in "${TESTS[@]}"; do
        echo "$testname" >> ${SKIPFILE}
    done
}

# $1: command/program to look for
# $2: string with busybox test names separated by spaces
# $3: This function may also be used in other tests, which can be considered as a general function of fuego.
function skip_if_command_unavailable {
    local PROGRAM="$1"
    local TESTS="$2"
    export FOUND=""

    is_on_target_path ${PROGRAM} FOUND
    if [ -z "${FOUND}" ]; then
        echo "WARNING: ${PROGRAM} is not installed on the target."
        skip_tests "${TESTS}"
    fi
}

# $1: string with busybox test names separated by spaces
function skip_if_not_root {
    if ! check_root ; then
        skip_tests "$1"
    fi
}

function test_pre_check {
    is_on_target_path busybox PROGRAM_BUSYBOX
    assert_define PROGRAM_BUSYBOX "Missing 'busybox' program on target board"
    echo "Tests skipped depending on the availability of a command on the target"
    touch ${LOGDIR}/skiplist.txt
    skip_if_command_unavailable expect "busybox_ash.sh busybox_passwd.sh"
    skip_if_command_unavailable tr "busybox_chgrp1.sh busybox_chgrp2.sh \
        busybox_chmod1.sh busybox_chmod2.sh busybox_chown1.sh busybox_chown2.sh"
    skip_if_not_root "busybox_chgrp1.sh busybox_chgrp2.sh busybox_chown1.sh \
        busybox_chown2.sh busybox_chroot.sh busybox_passwd.sh busybox_swap.sh"
}

function test_build {
    printf "\012\08\07\015\014" >string-data
    printf "Hello World" >>string-data
    printf "\012\08\07\015\02" >>string-data
    printf "\012\08\07\015\014" >>string-data
}


function test_deploy {
    put $TEST_HOME/busybox_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put ${LOGDIR}/skiplist.txt $BOARD_TESTDIR/fuego.$TESTDIR/
    put string-data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        export test_tcpip_host=$SRV_IP; \
        export test_port_host=${SRV_PORT:=8080}; \
        export test_ipaddr=$IPADDR;\
        sh -v busybox_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
