function test_pre_check {
    # check for required programs on host
    which flake8 || abort_job "Missing flake8 which is required for test"
#    which pylint || abort_job "Missing pylint which is required for test"
}

function test_run {
    # these tests are on the host

    log_this "echo ### Testing ftc with flake8 ###"
    set +e
    log_this "flake8 --count $FUEGO_CORE/engine/scripts/ftc"
    if [ "$?" == "0" ] ; then
        log_this "echo ok 1 flake8 test of ftc"
    else
        log_this "echo not ok 1 flake8 test of ftc"
    fi
    set -e

# disable the pylint test until I figure out to cleanly install pylint
# into the container
#    echo "### Testing ftc with pylint ###" >>$tmpfile
#    pylint $FUEGO_CORE/engine/scripts/ftc >>$tmpfile
#    if [ "$?" == "0" ] ; then
#        echo "ok 1 pylint test of ftc" >>$tmpfile
#    else
#        echo "not ok 1 pylint test of ftc" >>$tmpfile
#    fi

    # move this all to target, to use normal log retrieval
    # this is kind of awkward - we shuold have something like "report_local"
    log_this "echo Test done"
}

function test_processing() {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
