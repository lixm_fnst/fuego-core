# gitrepo=https://github.com/sahlberg/dbench.git
tarball=dbench-4.00.tar.gz

function test_pre_check {
    assert_define BENCHMARK_DBENCH_MOUNT_BLOCKDEV
    assert_define BENCHMARK_DBENCH_MOUNT_POINT
    assert_define BENCHMARK_DBENCH_TIMELIMIT
    assert_define BENCHMARK_DBENCH_NPROCS

    is_on_target_path dbench PROGRAM_DBENCH
    if [ ! -z "$PROGRAM_DBENCH" ]; then
        help=$(cmd "dbench --help") || true
        version=$(echo $help | head -1 | cut -d' ' -f 3)
        if version_lt $version 4.00 ; then
            echo "dbench found on the target but version $version is not supported."
            echo "Using Fuego's version of dbench."
            PROGRAM_DBENCH=""
        else
            echo "dbench found on the target (version $version)."
        fi
    fi
}

function test_build {
    if [ -z "$PROGRAM_DBENCH" ]; then
        # prepare zlib first
        tar xvf $TEST_HOME/zlib-1.2.11.tar.gz
        cd zlib-1.2.11/
        ./configure --static
        make
        # compile dbench using libz.a
        cd ..
        ./autogen.sh
        ./configure --host=$HOST
        cp ./zlib-1.2.11/zconf.h .
        LDFLAGS="-static -L./zlib-1.2.11/" make
    else
        echo "Skipping build phase, dbench is already on the target"
    fi
}

function test_deploy {
    if [ -z "$PROGRAM_DBENCH" ]; then
        put dbench loadfiles/client.txt $BOARD_TESTDIR/fuego.$TESTDIR/
    fi
}

function test_run {
    hd_test_mount_prepare $BENCHMARK_DBENCH_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH_MOUNT_POINT
    if [ -z "$PROGRAM_DBENCH" ]; then
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
            ./dbench -B fileio -c ./client.txt \
            -D $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR \
            -t $BENCHMARK_DBENCH_TIMELIMIT \
            $BENCHMARK_DBENCH_EXTRAPARAMS \
            $BENCHMARK_DBENCH_NPROCS; \
            sync"
    else
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
            dbench -D $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR \
            -t $BENCHMARK_DBENCH_TIMELIMIT \
            $BENCHMARK_DBENCH_EXTRAPARAMS \
            $BENCHMARK_DBENCH_NPROCS; \
            sync"
    fi
    hd_test_clean_umount $BENCHMARK_DBENCH_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH_MOUNT_POINT
}
