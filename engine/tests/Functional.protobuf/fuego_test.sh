tarball=protobuf-2.6.1.tar.gz

function test_build {
    echo "#!/bin/bash
    protoc --cpp_out=. --java_out=. --python_out=. addressbook.proto;

    if [ -f addressbook_pb2.py ] ; then
    echo 'TEST-1 OK'
    else
    echo 'TEST-1 FAILED'
    fi

    if [ -f addressbook.pb.cc ] ; then
    echo 'TEST-2 OK'
    else
    echo 'TEST-2 FAILED'
    fi

    if [ -f addressbook.pb.h ] ; then
    echo 'TEST-3 OK'
    else
    echo 'TEST-3 FAILED'
    fi

    if [ -d com ] ; then
    echo 'TEST-4 OK'
    else
    echo 'TEST-4 FAILED'
    fi" > run-tests.sh
}

function test_deploy {
    put examples/* run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh 2>&1"
}

function test_processing {
    log_compare "$TESTDIR" "4" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}


