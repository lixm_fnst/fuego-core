#!/usr/bin/python

import os, re, sys, random

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser') 
import common as plib


ref_section_pat = "\[[\w.]+.[gle]{2}\]"
cur_search_pat = re.compile("^(fuego_check_plots result:)(\ *)([\d]{1,8})(.*)$")

bbar_value = 900 + random.randint(-200, 200)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict["main.shell_random"] = pat_result[0][2]
	cur_dict["main.python_random"] = str(bbar_value)

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'fuego_check_plots results'))

