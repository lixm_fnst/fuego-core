tarball=lwip.tar.gz

function test_pre_check {
    is_on_target liblwip.so LIB_LWIP /lib:/usr/lib:/usr/local/lib
    assert_define LIB_LWIP
}

function test_build {
    [ -d build ] || mkdir build
    cd build
    cmake .. && make
    cd -
    cd contrib/apps/func_tests
    make
    cd -
    echo "#!/bin/bash
    export IP_TEST='192.168.1.3';
    if ./httpd_test > /dev/null & then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
    sleep 2;
    if wget \${IP_TEST}; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi;
    killall httpd_test

    if ./tftpd_test > /dev/null & then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi;
    sleep 2;
    if tftp -g -r Makefile \${IP_TEST}; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi;
    killall tftpd_test

    if ./mdns_test > /dev/null & then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi;
    sleep 1;
    killall mdns_test

    if ./snmp_test > /dev/null & then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi;
    sleep 1;
    killall snmp_test
    
    if ./sntp_test > /dev/null & then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi;
    sleep 1;
    killall sntp_test

" > run-tests.sh
}

function test_deploy {
    put build/lib/liblwip.a build/lib/liblwip.so build/lib/liblwip.so.2.0.0 \
        run-tests.sh contrib/apps/func_tests/*_test  $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh"  
}

function test_processing {
    log_compare "$TESTDIR" "7" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}


