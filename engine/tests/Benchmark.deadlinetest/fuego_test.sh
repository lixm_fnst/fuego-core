tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1
TEST_PROGRAM=deadline_test

function test_pre_check {
    assert_define BENCHMARK_DEADLINE_TEST_PARAMS
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 ${TEST_PROGRAM}
}

function test_deploy {
    put ${TEST_PROGRAM}  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./${TEST_PROGRAM} $BENCHMARK_DEADLINE_TEST_PARAMS"
}
