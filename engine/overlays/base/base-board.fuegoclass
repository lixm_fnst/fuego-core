OF.NAME="base-board"
OF.DESCRIPTION="Basic board file"
SSH_PORT="22"
SRV_IP="`/sbin/ip a s |awk -F ' +|/' '/inet / && $3 != "127.0.0.1" { print $3; exit; }'`"

# Make a stub function for setting up a board for a test.
# This can be any set of operations desired by the user,
# and should be overriden in the board file.
function ov_board_setup () {
  return
}

function ov_board_teardown () {
  return
}

# establish or validate connection to target
# log in if needed
# takes an option $1 which is number of retries to attempt
function ov_transport_connect () {
  local max_retries=1
  if [ -n "$1" ] ; then
    max_retries=$1
  fi

  case "$TRANSPORT" in
    local)
      return 0
      ;;
    ssh|ttc)
      retries=1
      while [ $retries -le $max_retries ] ; do
        ov_transport_cmd "true"
        if [ $? -eq 0 ] ; then
          return 0
        fi
        sleep 1
        retries=$(( $retries + 1 ))
      done
      return 1
      ;;
    serial)
      retries=1
      while [ $retries -le $max_retries ] ; do
        # use serlogin for logging in to the target board
        if [ -n ${PASSWORD} ] ; then
          $SERLOGIN -P "${PASSWORD}" $LOGIN
        else
          $SERLOGIN $LOGIN
        fi
        if [ $? -eq 0 ] ; then
          return 0
        fi
        sleep 1
        retries=$(( $retries + 1 ))
      done
      return 1
      ;;
    lava)
      # put LAVA connect here
      ;;
    *)
     abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT}"
     ;;
  esac
}

function ov_transport_disconnect () {
  case "$TRANSPORT" in
  ssh|ttc|local|serial)
    return 0
    ;;
  lava)
    # put LAVA disconnect here
    ;;
  *)
    abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT}"
    ;;
  esac
}

# $1 = remote file (source); $2 = local file (destination)
# I'm not sure what will happen with arguments past 2, but they're passed
# to scp unmodified.  I suspect this will confuse scp, and that all but
# the last will be treated as sources.
# FIXTHIS - ov_transport_get:  'ttc cp' doesn't support recursion
function ov_transport_get () {
  case "$TRANSPORT" in
  "ssh")
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${SCP_ARGS} -r $LOGIN@${DEVICE}:"$1" "${*:2}"
    else
      sshpass -e scp ${SCP_ARGS} -r $LOGIN@${DEVICE}:"$1" "${*:2}"
    fi
    ;;
  "ttc")
    $TTC $TTC_TARGET cp target:"$1" "${*:2}"
    ;;
  "local")
    if [ "$1" != "${*:2}" ]; then
        cp -r "$1" "${*:2}"
    fi
    ;;
  "serial")
    if startswith $SERIAL "/" ; then
        $SERCP -d $SERIAL serial:"$1" "${*:2}"
    else
        $SERCP $SERIAL:"$1" "${*:2}"
    fi
    wait
    ;;
  lava)
    # put LAVA get here
    # could be a recursive call with PLATFORM=ssh
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT}"
   ;;
  esac
}

# $1 = local file (source); $2 = remote file (destination)
# FIXTHIS - ov_transport_put:  'ttc cp' doesn't support multiple sources
# FIXTHIS - ov_transport_put:  'ttc cp' doesn't support recursion (-r argument)
function ov_transport_put () {
  case "$TRANSPORT" in
  "ssh")
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${SCP_ARGS} -r "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
    else
      sshpass -e scp ${SCP_ARGS} -r "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
    fi
    ;;
  "ttc")
    $TTC $TTC_TARGET cp "${@:1:$(($#-1))}" target:"${@: -1}"
    ;;
  "local")
    for par in "${@:1:$(($#-1))}"; do
        if [ "$par" != "-r" -a "$par" != "${@: -1}" ]; then
            cp -r "$par" "${@: -1}"
        fi
    done
    ;;
  "serial")
    if startswith $SERIAL "/" ; then
        $SERCP -d $SERIAL "${@:1:$(($#-1))}" "serial:${@: -1}"
    else
        $SERCP "${@:1:$(($#-1))}" "${SERIAL}:${@: -1}"
    fi
    wait
    ;;
  lava)
    # put LAVA put here
    # could be a recursive call with PLATFORM=ssh
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT}"
   ;;
  esac
}

# $@ = command and args for remote execution
function ov_transport_cmd() {
  case "$TRANSPORT" in
  "ssh")
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${SSH_ARGS}${DEVICE} "$@"
    else
      sshpass -e ssh ${SSH_ARGS}${DEVICE} "$@"
    fi
    ;;
  "ttc")
    $TTC $TTC_TARGET run "$@"
    ;;
  "local")
    /bin/sh -c "$@"
    ;;
  "serial")
    ${SERSH} "$@"
    wait
    ;;
  lava)
    # put LAVA cmd here
    # could be a recursive call with PLATFORM=ssh
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT}"
   ;;
  esac
}

# function to reboot the board
function ov_board_control_reboot() {
  if [ -z "$TTC_TARGET" ] ; then
    TTC_TARGET="$NODE_NAME"
  fi
  case "$BOARD_CONTROL" in
  "ttc")
    $TTC $TTC_TARGET reboot
    ;;
  *)
    abort_job "Error reason: unsupported BOARD_CONTROL ${BOARD_CONTROL}"
    ;;
  esac
}
